
/* libc include for the printf function */
#include <stdio.h> 

/* libc include for the memory functions */
#include <stdlib.h>

/* libc include for the string functions */
#include <string.h>

/* libc include for the char functions */
#include <ctype.h>

/* libc include for the header file */
#include "header.h"





/*
 *@brief
 *
 *@param
 *@return
 */
extern int setup(int notation){
	
	const char names[2][24][10] = {
		{"C", "C+/D-", "D", "D+/E-", "E", "F", "F+/G-", "G", "G+/A-", "A", "A+/B-", "B", "C", "C+/D-", "D", "D+/E-", "E", "F", "F+/G-", "G", "G+/A-", "A", "A+/B-", "B" },
		{"Do", "Do+/Re-", "Re", "Re+/Mi-", "Mi", "Fa", "Fa+/Sol-", "Sol", "Sol+/La-", "La", "La+/Si-", "Si", "Do", "Do+/Re-", "Re", "Re+/Mi-", "Mi", "Fa", "Fa+/Sol-", "Sol", "Sol+/La-", "La", "La+/Si-", "Si" }
		};

	extern struct note{
		int num;
		char noteName[10];
		char wavName[15];
		};

	extern struct note notes[24];

	int i;
	for (i=0; i<24; i++){
		notes[i].num=i+1;
		sprintf(notes[i].wavName,"sample%2d.wav", notes[i].num);
		sprintf(notes[i].noteName,"%s", names[notation][i]);
	}
	
	printf("####DEBUG:Setup####");
	return 0;
	}


/*
 *@brief
 *
 *@param
 *@return
 */
extern short int askNotation(){

	char choice;
	
	
	printf("\n\n|Please enter favorite notation system:\n");
	printf("|[T] for traditional notation(Do, Re, Sol#)]\n|[L] for literal notation (C, D, G#)]\n|-->");
	choice=getchar();
	while(getchar()!='\n');
	
      if(choice=='t'||choice=='T'){
		printf("\ntraditional notation adopted\n");
		return 0;
		}else if(choice=='l'||choice=='L'){
			printf("\nliteral notation adopted\n");
			return 1;
		}else{
			printf("\nInvalid character, using literal notation instead\n");
			return 1;
		}
	}
	
extern short int mainMenu(short int notation)
	{
	short int run=1;
	char op;
	
	while(run)
		{
		printf("\n|Enter:\n|[C] to start Chord Generator\n|[S] to start Scale Generator \n|[Q] to quit\n|-->");
		
		op=getchar();
		while(getchar()!='\n');

		if(op=='C'||op=='c')
			{
			printf("\nStarting Chord Generator\n");
			generateChord(notation);
			}else if(op=='S'||op=='s')
				{
				printf("\nStarting Scale Generator\n");
				mainScale(notation);
			}else if(op=='Q'||op=='q')
				{
				printf("\nQuitting...\n");
				run=0;
			}else
				{
				printf("\nInvalid operation character\n");
			}
		}
	return run;
	}

