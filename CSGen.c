/**
 * @file 
 * @brief 
 * 
 * @author 
 * @version 1.0
 * @since 1.0
 *
 * @copyright Copyright (c) 2016-2017 University of Padua, Italy
 * @copyright Apache License, Version 2.0
 */

/* libc include for the printf function */
#include <stdio.h> 

/* libc include for the memory functions */
#include <stdlib.h>

/* libc include for the string functions */
#include <string.h>

/* libc include for the char functions */
#include <ctype.h>

/* libc include for the header file */
#include "header.h"

/**
 * @brief 
 * @detail 
 * 
 * @param 
 *
 * @return 
 *
 * @author 
 * @version 1.0
 * @since 1.0
 */
int main(void) {
	
	setup();
		
	printf("\n-----Welcome to the Chords&Scales Generator!-----\a");
	
	int notation=askNotation();

	mainMenu(notation);
	
		
	return 0;
}


















