
/* libc include for the printf function */
#include <stdio.h> 

/* libc include for the memory functions */
#include <stdlib.h>

/* libc include for the string functions */
#include <string.h>

/* libc include for the char functions */
#include <ctype.h>

#if defined(_WIN32)
	#include<Windows.h>
#endif

int main(void) {
	int flag;
	#if defined(__APPLE__) && defined(__MACH__)
		printf("executing afplay");
		flag=system("aplay -c 1 -t wav -q test.wav");
	#elif defined(__linux__)
		printf("executing aplay");
		flag=system("aplay -c 1 -t wav -q test.wav");
	#elif defined(_WIN32)	
		printf("executing PlaySound");
		flag=PlaySound(TEXT("test.wav"), NULL, SND_SYNC);
	#endif
	
	return flag;
}

