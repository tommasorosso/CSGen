/*
 *@file header.h
 *@brief
 *
 *@author
 *@version
 *@since
 *
 *@copyright
 *@copyright
 */



/*
 *@brief
 *
 *@param
 *@return
 */
extern int setup();

/*
 *@brief
 *
 *@param
 *@return
 */
extern int generateChord(int notation);

/*
 *@brief
 *
 *@param
 *@return
 */
extern int mainScale(int notation);


/*
 *@brief
 *
 *@param
 *@return
 */
extern short int askNotation();


/*
 *@brief
 *
 *@param
 *@return
 */
extern short int mainMenu(short int notation);

